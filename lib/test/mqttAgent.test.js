const { expect } = require('chai');
const MQTTAgent = require('../index.js');
const config = require('../../config.json');

describe('MQTTAgent', () => {
  const mqttServer = config.mqtt_server;
  const testTopic = config.topic;
  const msgJson = config.message;

  it('can connect to MQTT Server', (done) => {
    const client = new MQTTAgent(mqttServer);
    client.connect()
      .then((connected) => {
        expect(connected).to.equal(true);
        done();
      }).catch((error) => {
        done(error);
      });
  });

  it('can can return if it is connected', (done) => {
    const client = new MQTTAgent(mqttServer);
    client.connect()
      .then(() => {
        expect(client.isConnected()).to.equal(true);
        done();
      }).catch((error) => {
        done(error);
      });
  });

  it('can subscribe to a topic', (done) => {
    const client = new MQTTAgent(mqttServer);
    client.connect()
      .then((connected) => {
        expect(connected).to.equal(true);
        return client.subscribe(testTopic);
      }).then((topic) => {
        expect(topic).to.be.a('string');
        expect(topic).to.equal(testTopic);
        done();
      }).catch((error) => {
        done(error);
      });
  });

  it('can publish a message into a topic', (done) => {
    const client = new MQTTAgent(mqttServer);
    client.connect()
      .then((connected) => {
        expect(connected).to.equal(true);
        return client.publish(testTopic, msgJson);
      }).then((published) => {
        expect(published).to.equal(true);
        done();
      }).catch((error) => {
        done(error);
      });
  });

  it('can receive a message from a topic', (done) => {
    function onMessageListener(topic, message) {
      const parsedMessage = JSON.parse(message);
      expect(parsedMessage).to.be.an('object');
      done();
    }

    const client = new MQTTAgent(mqttServer, onMessageListener);
    client.connect()
      .then((connected) => {
        expect(connected).to.equal(true);
        return client.subscribe(testTopic);
      }).then((topic) => {
        expect(topic).to.be.a('string');
        expect(topic).to.equal(testTopic);
        return client.publish(testTopic, msgJson);
      }).catch((error) => {
        done(error);
      });
  });

  it('can unsubscribe from topic', (done) => {
    const client = new MQTTAgent(mqttServer);
    client.connect()
      .then((connected) => {
        expect(connected).to.equal(true);
        return client.subscribe(testTopic);
      }).then((topic) => {
        expect(topic).to.be.a('string');
        expect(topic).to.equal(testTopic);
        return client.unsubscribe(testTopic);
      }).then((unsubscribedTopic) => {
        expect(unsubscribedTopic).to.be.a('string');
        expect(unsubscribedTopic).to.equal(testTopic);
        done();
      })
      .catch(done);
  });

  it('can unsubscribe from array of topic', (done) => {
    const client = new MQTTAgent(mqttServer);
    client.connect()
      .then((connected) => {
        expect(connected).to.equal(true);
        return client.subscribe(testTopic);
      }).then(() => client.unsubscribe([testTopic]))
      .then((topic) => {
        expect(topic).to.be.an('array');
        expect(topic).to.eql([testTopic]);
        done();
      })
      .catch(done);
  });

  it('can close the connection', (done) => {
    const client = new MQTTAgent(mqttServer);
    client.connect()
      .then((connected) => {
        expect(connected).to.equal(true);
        return client.end();
      }).then((disconnected) => {
        expect(disconnected).to.equal(true);
        done();
      }).catch(done);
  });
});
