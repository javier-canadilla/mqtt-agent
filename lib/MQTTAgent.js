const mqtt = require('mqtt');

/**
 * @constructor
 * @param {string} uri Url server
 * @param {Function} onMessage Callback
 */
function MQTTAgent(uri, onMessage = null, onClose = null) {
  this.uri = uri;
  this.client = undefined;
  this.topics = []; // Subscribed Topics
  this.errorOcurred = false;
  this.onMessage = (onMessage || this.message);
  this.onClose = (onClose || this.reconnect);
  this.connectOptions = undefined;
}

// When the client is connected we can use Publish, suscribe, etc...
// We have to implement a listener or callback to do something with the message
/**
 * Create a connecion
 */
MQTTAgent.prototype.connect = function connect(options = null, onMessage = null) {
  if (onMessage) this.onMessage = onMessage;
  this.connectOptions = options;
  const that = this;
  this.client = mqtt.connect(that.uri, options);
  return new Promise((resolve, reject) => {
    that.client.on('message', that.onMessage);
    that.client.on('connect', () => {
      resolve(true);
    });
    that.client.on('error', reject);
    that.client.on('close', that.onClose);
  });
};

function checkJSON(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

MQTTAgent.prototype.message = function message(topic, msg) {
  checkJSON(msg);
  let msgObj = msg;
  let isJson = false;
  if (checkJSON(msg)) {
    isJson = true;
    msgObj = JSON.parse(msg);
  }
  if (isJson) {
    console.log(`TOPIC: ${topic} - MESSAGE: ${JSON.stringify(msgObj)}`);
  } else {
    console.log(`TOPIC: ${topic} - MESSAGE: ${msgObj.toString()}`);
  }
};

MQTTAgent.prototype.error = function error(err) {
  if (err) {
    this.errorOcurred = true;
    throw err;
  }
};

MQTTAgent.prototype.reconnect = function reconnect() {
  console.log('Connection lost... Reconnecting!');
  this.reconnect(this.connectOptions);
};

MQTTAgent.prototype.isConnected = function isConnected() {
  return this.client.connected;
};

/**
 * Subscribe to a topic/topics
 * @param {(string | string[])} topics Topic subscribe to
 * @param {Object=} options Object with options
 * @returns {Boolean}
 */
MQTTAgent.prototype.subscribe = function subscribe(topics, options = undefined) {
  if (this.isConnected()) {
    if (typeof (topics) === 'string' || typeof (topics) === 'object' || Array.isArray(topics)) {
      const that = this;

      return new Promise((resolve, reject) => {
        this.client.subscribe(topics, options, (error, granted) => {
          if (error) reject(error);
          that.topics.push(granted[0].topic);
          resolve(granted[0].topic);
        });
      });
    }
    throw new Error('The input topics are incorrect.');
  } else {
    throw new Error('You are not connected to a MQTT server.');
  }
};

/**
 * Send a JSON or String
 * @param {(string | string[])} topic
 * @param {(Object | string)} msg
 * @param {Object=} options
 * @return {Boolean}
 */
MQTTAgent.prototype.publish = function publish(topic, msg, options = null) {
  if (this.isConnected()) {
    if (typeof (msg) !== 'object' && typeof (msg) !== 'string') throw new Error('The message have to be a string or an object.');
    return new Promise((resolve, reject) => {
      let finalMsg = msg;

      // If msg is an object, we put it into a buffer.
      if (typeof (msg) === 'object') {
        finalMsg = new Buffer.from(JSON.stringify(msg));
      }

      this.client.publish(topic, finalMsg, options, (error) => {
        if (error) reject(error);
        resolve(true);
      });
    });
  }
  throw new Error('You are not connected to a MQTT server.');
};

MQTTAgent.prototype.unsubscribe = function unsubscribe(topic) {
  if (this.isConnected()) {
    if (typeof (topic) === 'string' || Array.isArray(topic)) {
      if (typeof (topic) === 'string') {
        const indx = this.topics.indexOf(topic);
        if (indx > -1) {
          this.topics.splice(indx, 1);
        }
      } else {
        const finalTopics = this.topics.filter(item => topic.indexOf(item) === -1);
        this.topics = finalTopics;
      }

      return new Promise((resolve, reject) => {
        this.client.unsubscribe(topic, (error) => {
          if (error) reject(error);
          resolve(topic);
        });
      });
    }
    throw new Error('The input topics are incorrect.');
  } else {
    throw new Error('You are not connected to a MQTT server.');
  }
};

MQTTAgent.prototype.end = function end(force = false) {
  return new Promise((resolve, reject) => {
    this.client.end(force, (error) => {
      if (error) reject(error);
      resolve(true);
    });
  });
};

module.exports = MQTTAgent;
