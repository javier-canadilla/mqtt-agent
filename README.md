# This is a MQTT Agent

It is a simplification of MQTT in NodeJS. All functions return a native-promise that you can use Asynchronously.

## Installation

```sh
$ npm install --save mqtt-agent
```

## Usage

```js
const mqttAgent = require('mqtt-agent');

// const agent = new MQTTAgent('mqtt://yourmqttserver:8883'); // Default message callback
const agent = new MQTTAgent('mqtt://your-mqtt-server:8883', (topic, msg) => {
  // Do something with your Topic and Message.
}); // Custom message callback

agent.connect() // Return a promise when it is connected to your server
  .then((connected) => {
    console.log('Connected:', connected); // Do something with the connected status
    // You can now subscribe, publish and all with native promises!
    return agent.subscribe('your_topic/your_device/something'); // Returns a promise
  })
  .then((subscribedTopic) => {
    console.log('Topic subscribed to:', subscribedTopic);
    // You can send options as last parameter
    return agent.publish('yourTopic', 'yourMessage can be a JS Object or string'); 
  })
  .then((published) => {
    console.log('Published (boolean):', published); // Then if you are subscribed to the topic, you will receive the message in your message callback
    return agent.end(); // Disconnect
  })
  .then((disconnected) => {
    console.log('Disconnected:', disconnected);
  })
  .catch((error) => {
    console.log('Error:', error);
  });

```
## License

 © [Javier Cañadilla]()
