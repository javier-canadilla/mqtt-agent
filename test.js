const MQTTAgent = require('./lib/index');

let count = 0;
let time = 0;
function printPrettyMessageCounter() {
  time += 1;
  console.clear();
  console.log('COUNT:', count);
  console.log('COUNT/sec:', (count / time).toFixed(2));
  console.log('TIME:', `${time} segundos.`);
}

function getMessage() {
  count += 1;
}

const client = new MQTTAgent('mqtt://yourmqttserver.com:8883', getMessage);

client.connect()
  .then((connected) => {
    console.log('Connected:', connected);
    return client.subscribe('/counter/#');
  }).catch((error) => {
    console.log('ERROR:', error);
  });

setInterval(printPrettyMessageCounter, 1000);
